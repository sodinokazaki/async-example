
const toggleloadMessage = function() {
    let domObj = document.getElementById("animalText");
    domObj.innerHTML = "<div id='loader'><span>Please Wait</span></div>";
};

function callService (){
    toggleloadMessage();
    service.getAsync().then();
}

function callServicePromise (){
    toggleloadMessage();
    service.getPromise();
}



/**
 * Created by nolan.docherty on 31/03/2017.
 */
let service = (function () {

    // this code uses Async

    const webRequestAsync = (webApi) => {
        // this is a syncronous request which your not 'meant' to use anymore but I'm only using it to
        // show the async functionality

        let basicUrl = "http://apiv3.iucnredlist.org/api/v3/";
        let token = "?token=9bb4facb6d23f48efbf424bb05c0c1ef1cf6f468393bc745d42179ac4aca5fee";
        let request = new XMLHttpRequest();
        request.open('POST', basicUrl + webApi + token, false);  // `false` makes the request synchronous
        request.send(null);

        if (request.status === 200) {
            return JSON.parse(request.responseText);
        } else {
            return null;
        }
    };


    const countriesCall = () => {
        //call the web
        let response = webRequestAsync("country/list");
        return response.results;

    };

    const animalCall = (country) => {

        let result = webRequestAsync(`country/getspecies/${country.isocode}`);
        return result.result;
    };

    const animalInfoCall = (speciesObj) => {
        return webRequestAsync(`species/narrative/${speciesObj.scientific_name}`);
    };


    const getRndNo = (array) => {
        let range = array.length + 1;
        let myRndNo = Math.floor(Math.random() * range);
        return array[myRndNo];
    };


    const postInfo = function (animalInfo) {
        const div = document.getElementById("animalText");
        let text = `<div class="animal-name"><h1>Animals latin name:  ${animalInfo.name}</h1></div>`;
        const infoList = animalInfo.result[0];
        for (let line in  infoList) {
            if (infoList[line] !== null) {
                text += `<div>
 <h2>${line}:</h2> ${infoList[line]}
</div>
        `
            }
        }
        div.innerHTML = text;
    };

    const getAsync = async  () => {

        let countries = await countriesCall();

        let randomCountry = getRndNo(countries);

        let animals = await animalCall(randomCountry);

        let randomAnimal = getRndNo(animals);

        let animalInfo = await animalInfoCall(randomAnimal);

        postInfo(animalInfo);

        return null;
    }


    // This code  uses Promises

    const webRequestPromise = (webApi) => {
        // this is a syncronous request which your not 'meant' to use anymore but I'm only using it to
        // show the async functionality

        let basicUrl = "http://apiv3.iucnredlist.org/api/v3/";
        let token = "?token=9bb4facb6d23f48efbf424bb05c0c1ef1cf6f468393bc745d42179ac4aca5fee";


        return new Promise(function (resolve, reject) {
            // Do the usual XHR stuff
            let request = new XMLHttpRequest();
            request.open('POST', basicUrl + webApi + token);  // `false` makes the request synchronous

            request.onload = function () {
                // This is called even on 404 etc
                // so check the status
                if (request.status == 200) {
                    // Resolve the promise with the response text
                    resolve(JSON.parse(request.response));
                }
                else {
                    // Otherwise reject with the status text
                    // which will hopefully be a meaningful error
                    reject(Error(request.statusText));
                }
            };

            // Handle network errors
            request.onerror = function () {
                reject(Error("Network Error"));
            };

            // Make the request
            request.send();
        });
    };

    const countriesCallPromise = () => {
        webRequestPromise("country/list").then(function (response) {
            let country = getRndNo(response.results);
            animalCallPromise(country);
        }).catch( function () {return "There was an error"});
    };
    const animalCallPromise = (country) => {
        webRequestPromise(`country/getspecies/${country.isocode}`)
            .then(function (response) {
                let randomeAnimal = getRndNo(response.result);
                animalInfoCallPromise(randomeAnimal);
            }).catch( function () {return "There was an error"});
    };

    const animalInfoCallPromise = (speciesObj) => {
        webRequestPromise(`species/narrative/${speciesObj.scientific_name}`)
            .then(function (animalInfo) {
                postInfo(animalInfo);
            }).catch( function () {return "There was an error"});
    }

    const getPromise = () => {

        countriesCallPromise();

    }

    return {
        getAsync,
        getPromise,
    }
})
();
